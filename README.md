# InkWell-admin

Hello and welcome to the Inkwell documentation.  
Get a overview of the project and how to set things up as we intended it for you.

#### Requirements

For fast and streamlined development we have set up this project using Node, Grunt and Bower.  
Please install these requirements to enjoy the full potential of this theme.

#### Node

First make sure you have [Node.js](https://nodejs.org/ "Node.js") installed.  
When you have successfuly installed Node you should see something similar when running the commands:

```sh
$ node --version
v5.1.1
$ npm --version
3.5.1
```

#### Grunt and Bower

Now that we have Node and NPM we can install Grunt and Bower which will help us compile (CSS), uglify (JS) and do all sort of usefull tasks.

```sh
$ npm install -g grunt-cli bower
```

#### Install requirements for Materialism with Grunt and Bower

With Bower you download the required assets (Angular, Bootstrap etc) and with NPM you download the Grunt dependencies (SASS).

```sh
$ cd /to/path/where Gruntfile.js and bower.json are located
$ npm install
$ bower install
```

Sometimes when running Bower a version mismatch can occur between certain assets. When this happens for Angular always choose the version used by inkwell-admin.

```sh
Unable to find a suitable version for angular, please choose one:
    1) angular#~1.3 which resolved to 1.3.20 and is required by c3-angular#0.7.1
    2) angular#1.4.8 which resolved to 1.4.8 and is required by angular-animate#1.4.8
    3) angular#^1.2 which resolved to 1.4.8 and is required by angular-simple-logger#0.0.4
    4) angular#1.2 - 1.4 which resolved to 1.4.8 and is required by angular-google-maps#2.2.1

Prefix the choice with ! to persist it to bower.json

? Answer 2
```

In this case we awnser with: 2

> **From here on out when we mention to run a command, do so from the project root (Where Gruntfile.js is located).**


## Development

#### Viewing the inkwell-admin

To view the theme start a local webserver with grunt using:

```sh
$ grunt
```

This will start a local webserver on port 9000. Open one of the following urls in your browser:

- http://localhost:9000
