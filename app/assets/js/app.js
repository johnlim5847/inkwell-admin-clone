/*jslint strict: true */

var app = angular.module('materialism', [
  'app.constants',
  'firebase',
  'ngFlash',
  'materialism.home',
  'ipCookie',
  'angularjs-datetime-picker',
  'moment-picker',
  'ngCookies',
  'ng-token-auth',
  'ngRoute',
  'ngAnimate',
  'ngSanitize',
  'ngPlaceholders',
  'ngTable',

  'angular-loading-bar',

  'angulartics',
  'angulartics.google.analytics',

  'nemLogging',
  'uiGmapgoogle-maps',
  'ui.select',

  'gridshore.c3js.chart',
  'monospaced.elastic',     // resizable textarea
  'mgcrea.ngStrap',
  'jcs-autoValidate',
  'ngFileUpload',
  'textAngular',
  'fsm',                    // sticky header
  'smoothScroll',
  'LocalStorageModule'
]);