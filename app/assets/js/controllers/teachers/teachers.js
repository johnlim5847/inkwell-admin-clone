app.controller('TeacherController', ['$scope', "$firebaseObject", "$firebaseArray", 'ngTableParams', '$filter','$routeParams', 'Flash', 'teacherService', '$location', function($scope, $firebaseObject, $firebaseArray, ngTableParams, $filter, $routeParams, Flash, teacherService, $location){

  // Firebase Setup
  var ref = firebase.database().ref();
  var list = $firebaseArray(ref);
  var obj = $firebaseObject(ref);
  $('.loaderdiv').show();
  $('.studentData').hide();
  // to take an action after the data loads, use the $loaded() promise
  obj.$loaded().then(function() {
    if($routeParams.tpl == "basic-info"){ 
      $scope.infoDiv = true;
      $scope.teachersDiv = false;      
    }else if($routeParams.folder == "teachers"){
      $scope.infoDiv = false;
      $scope.teachersDiv = true; 
    }
    $scope.staffs = obj.staff;
    $scope.sessions = obj.session;
    $scope.statuses = ["active", "archive", "passive", "pending"];
    $scope.types = ["Owner", "Admin", "Teacher"];
    $('.loaderdiv').hide();
    $('.studentData').show();
    $scope.studentStatusActive = true;
    $scope.studentStatusArchive = true;
    $scope.studentStatusPending = true;
    $scope.studentStatusPassive = true;
    var data = [];
    var sessions = obj.session;
    var count = 0;
    $scope.value_id = $routeParams.id;
    $scope.value = teacherService.getTeachers();
    if($routeParams.folder == "teachers"){
      for (var key in obj.staff){
        if($routeParams.id == key){
          $scope.value = obj.staff[key];
        }
        data.push({
            name: obj.staff[key].name,
            type: obj.staff[key].type,
            status: obj.staff[key].status,
            id: key
          });
      }
    }
    dataList($scope, ngTableParams, $filter, data);
    $scope.modalValue = function(item){
      $scope.value = item;
      $scope.item = item.id;
    };
    $scope.tab_change = function(item){
      console.log(item);
      teacherService.addTeacher(item);
      $('.student-tab').on('click',function(){
        $(this).addClass("active");
      });
    };
    $scope.delete = function(item){
      $scope.data = data;
      delete obj.staff[item.id];   
      $scope.data.splice($scope.data.indexOf(item), 1);
      $scope.tableParams.reload();
      Flash.create('success', "Teacher Successfully Deleted", 4000, {}, false);
      obj.$save("staff").then(function (ref) {
        console.log("Saved !");
      }, function (error) {
        console.log("Error:", error);
      });
    };
    $scope.changeSidebar = function(item){
      teacherService.addTeacher(item, item.id);
      $scope.value = item;
      $scope.infoDiv = true;
      $scope.teachersDiv = false;
      $location.path( "/teachers/"+item.id+"/basic-info" );
    };
    $scope.TeacherInfoSave = function(value, id){
      if (typeof id == 'undefined'){
        var newObj = {};
        newObj = {
          name: value.name,
          type: value.type,
          status: value.status
        };
        obj.staff[value.name+value.type] = newObj;
        $scope.data.push(newObj);
        $scope.tableParams.reload();
      }else{
        // console.log(value.subject);
        obj.staff[id].name = value.name;
        obj.staff[id].type = value.type;
        obj.staff[id].status = value.status;
      }
      obj.$save("staff").then(function (ref) {
        console.log("Saved !");
      }, function (error) {
        console.log("Error:", error);
      });
      var message = '';
      if(typeof id == 'undefined'){
        message = 'Teacher Successfully Created';
      }else{
        message = 'Teacher Successfully Updated';
      }
      Flash.create('success', message, 4000, {}, false);
    };


  $scope.selection = ['active', 'archive'];
  $scope.getCheckedActive = function(){
    return true;
  };
  $scope.getCheckedArchive = function(){
    return true;
  };
  // Toggle selection for a given fruit by name
  $scope.toggleSelection = function toggleSelection(active, pending, passive, archive) {
    teatureStatusList($scope, ngTableParams, true, data, active, pending, passive, archive);
  };
  });
}]);
function teatureStatusList($scope, ngTableParams, $filter, data, active, pending, passive, archive){
  var mydata = [];
  mydata = data.filter(function(item){
    if(active && archive && pending && passive){
      return true;
    }else if(archive && item.status.toLowerCase() == "archive"){
      return true;
    }else if(active && item.status.toLowerCase() == "active"){
      return true;
    }else if(pending && item.status.toLowerCase() == "pending"){
      return true;
    }else if(passive && item.status.toLowerCase() == "passive"){
      return true;
    }
  });
  $scope.tableParams = new ngTableParams({
    page: 1,            // show first page
    count: 10           // count per page
  },{
    total: data.length, // length of data
    getData: function($defer, params) {
      $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
    }
  });
}

function dataList($scope, ngTableParams, $filter, data){
  $scope.data = data;
  $scope.tableParams = new ngTableParams({
    page: 1,            // show first page
    count: 10,
    sorting: {
      name: 'asc'     // initial sorting
    }
  }, {
    filterDelay: 50,
    total: data.length, // length of data
    getData: function($defer, params) {
      var searchStr = params.filter().search;
      var mydata = [];
      if(searchStr){
        searchStr = searchStr.toLowerCase();
        mydata = data.filter(function(item){
          return item.name.toLowerCase().indexOf(searchStr) > -1;
        });
      } else {
        mydata = data;
      }
      mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
      $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
    }
  });
}

// active tabs
function tab_active() {
  $('.student-tab').on('click',function(){
      $(this).addClass("active");
  });
}
app.directive('ngConfirmClick', [
  function(){
    return {
      link: function (scope, element, attr) {
      var msg = attr.ngConfirmClick || "Are you sure?";
      var clickAction = attr.confirmedClick;
      element.bind('click',function (event) {
      if ( window.confirm(msg) ) {
        scope.$eval(clickAction);
      }
    });
  }
 };
}]);