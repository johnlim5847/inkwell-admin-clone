app.controller('TeacherSessionController', ['$scope', "$firebaseObject", "$firebaseArray", 'ngTableParams', '$filter','$routeParams', 'Flash', 'teacherService', function($scope, $firebaseObject, $firebaseArray, ngTableParams, $filter, $routeParams, Flash, teacherService){

  // Firebase Setup
  var ref = firebase.database().ref();
  var list = $firebaseArray(ref);
  var obj = $firebaseObject(ref);
  $scope.infoDiv = true;
  $('.loaderdiv').show();
  $('.studentData').hide();
  // to take an action after the data loads, use the $loaded() promise
  obj.$loaded().then(function() {
    $scope.value = teacherService.getTeachers();
    $scope.subjects = list[0][Object.keys(obj.organisation)[0]].config.subjects;
    $scope.levels = list[0][Object.keys(obj.organisation)[0]].config.levels;
    $scope.students = obj.student;
    // console.log(obj.staff);
    $scope.sessions = obj.session;
    $('.loaderdiv').hide();
    $('.studentData').show();
    var data = [];
    var sessions = obj.session;
    var count = 0;
    $scope.value_id = $routeParams.id;
    if($routeParams.tpl == "sessions"){
      for (var key in obj.session){
        if($routeParams.id == obj.session[key].staffId){
          $scope.value = obj.student[obj.session[key].studentId];
          data.push({
            timeStart: new Date(obj.session[key].timeStart)*1000,
            subject: obj.session[key].subject,
            studentId: obj.student[obj.session[key].studentId],
            level: obj.session[key].level,
            units: obj.session[key].units,
            id: key
          });
        }
      }
    }
    dataList($scope, ngTableParams, $filter, data);
    $scope.modalValue = function(item){
      $scope.value = item;
      $scope.item = item.id;
    };
    $scope.setFormattedDate = function (momentDate) {
       // do you stuff.. and
       $scope.value.formattedDate = momentDate.format('DD MMM YYYY');
    };
    if($routeParams.startDate && $routeParams.endDate) {
      $scope.startingDate = $routeParams.startDate;
      $scope.endingDate = $routeParams.endDate;
      dateSort($scope, ngTableParams, $scope.startingDate, $scope.endingDate, data);
    }
    $scope.dateSearch = function(starting, ending){
      if(starting && ending){
        dateSort($scope, ngTableParams, starting, ending, data);
      }
    };
    $scope.tab_change = function(item){
      teacherService.addTeacher(item);
      $('.student-tab').on('click',function(){
        $(this).addClass("active");
      });
    };
    $scope.delete_value = function(){
      $scope.value = "";
    };
    $scope.SessionInfoSave = function(value, id){
      var studentKey = 0;
      for (var key in obj.student){
        if(obj.student[key].name == value.studentId.name){
          studentKey = key;
        }
      }
      console.log($routeParams.id);
      if (typeof id == 'undefined'){
        var newObj = {};
        console.dir(value.timeStart);
        newObj = {
          timeStart: Date.parse(value.timeStart)/1000,
          subject: value.subject,
          studentId: studentKey,
          level: value.level,
          staffId: $routeParams.id,
          units: value.units
        };
        obj.session[value.timeStart+value.school+value.units] = newObj;
      }else{
        if(Date.parse(value.timeStart)){
          obj.session[id].timeStart = Date.parse(value.timeStart)/1000;
        }
        obj.session[id].subject = value.subject;
        obj.session[id].studentId = studentKey;
        obj.session[id].level = value.level;
        obj.session[id].units = value.units;
      }
      obj.$save("session").then(function (ref) {
        if (typeof id == 'undefined'){
          newObj.timeStart = Date.parse(value.timeStart);
          $scope.data.push(newObj);
          $scope.tableParams.reload();
        }
        console.log(ref);
        console.log("Saved !");
      }, function (error) {
        console.log("Error:", error);
      });
      var message = '';
      if(typeof id == 'undefined'){
        message = 'Session Successfully Created';
      }else{
        message = 'Session Successfully Updated';
      }
      Flash.create('success', message, 4000, {}, false);
    };


  $scope.selection = ['active', 'archive'];
  $scope.getCheckedActive = function(){
    return true;
  };
  $scope.getCheckedArchive = function(){
    return true;
  };
  // Toggle selection for a given fruit by name
  $scope.toggleSelection = function toggleSelection(status) {
    var idx = $scope.selection.indexOf(status);
    console.log(idx);
    // Is currently selected
    if (idx > -1) {
      if(status == 'active'){
        $scope.getCheckedArchive = function(){
          return true;
        };
      }
      if(status == 'archive'){
        $scope.getCheckedActive = function(){
          return true;
        };
      }
      statusList($scope, ngTableParams, status, data, false);
      $scope.selection.splice(idx, 1);
    }
    // Is newly selected
    else {
      if(status == 'active'){
        $scope.getCheckedArchive = function(){
          return false;
        };
      }
      if(status == 'archive'){
        $scope.getCheckedActive = function(){
          return false;
        };
      }
      statusList($scope, ngTableParams, status, data, true);
      $scope.selection.push(status);
    }
  };
  });
}]);
// function statusList($scope, ngTableParams, value, data, checked){
//   var mydata = [];
//   mydata = data.filter(function(item){
//     if(checked){
//       return item.status.toLowerCase() == value.toLowerCase();
//     }else{
//       return item.status.toLowerCase() != value.toLowerCase();
//     }
//   });
//   $scope.tableParams = new ngTableParams({
//     page: 1,            // show first page
//     count: 10           // count per page
//   },{
//     total: data.length, // length of data
//     getData: function($defer, params) {
//       $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
//     }
//   });
// }
function dateSort($scope, ngTableParams, starting, ending, data){
  // console.log(ngTableParams);
  var mydata = [];
  mydata = data.filter(function(item){
    var sessionDate = new Date(item.timeStart);
    sessionDate.setHours(0, 0, 0);
    return (sessionDate >= starting && sessionDate <= ending);
  });
  $scope.tableParams = new ngTableParams({
    page: 1,            // show first page
    count: 10           // count per page
  },{
    total: data.length, // length of data
    getData: function($defer, params) {
      $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
    }
  });
}
function dataList($scope, ngTableParams, $filter, data){
  $scope.data = data;
  $scope.tableParams = new ngTableParams({
    page: 1,            // show first page
    count: 10,
    sorting: {
      name: 'asc'     // initial sorting
    }
  }, {
    filterDelay: 50,
    total: data.length, // length of data
    getData: function($defer, params) {
      var searchStr = params.filter().search;
      var mydata = [];
      if(searchStr){
        searchStr = searchStr.toLowerCase();
        mydata = data.filter(function(item){
          return item.name.toLowerCase().indexOf(searchStr) > -1;
        });
      } else {
        mydata = data;
      }
      mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
      $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
    }
  });
}

// active tabs
function tab_active() {
  $('.student-tab').on('click',function(){
      $(this).addClass("active");
  });
}
