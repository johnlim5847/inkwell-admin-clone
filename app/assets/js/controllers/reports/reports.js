app.controller('ReportController', [
  '$scope',
  "$firebaseObject",
  "$firebaseArray",
  'ngTableParams',
  '$filter',
  '$location',
  '$routeParams',
  function($scope, $firebaseObject, $firebaseArray, ngTableParams, $filter, $location, $routeParams) {
    // Firebase Setup
    var ref = firebase.database().ref();
    var obj = $firebaseObject(ref);
    obj.$loaded().then(function() {
      var data = [];
      for (var key in obj.staff) {
        if ($routeParams.id == key) {
          $scope.value = obj.staff[key];
        }
        data.push({
          name: obj.staff[key].name,
           id: key,
           orgId: obj.staff[key].orgId
         });
      }
      //search here.
      $scope.dateSearch = function(starting, ending){
        if (ending < starting) {
          $scope.validationError = "Start Date is greater than End Date";
          return;
        }
        if(starting && ending){
          filterSessionByStartTime(starting, ending).then(function(sessions){
            createTableData($scope, ngTableParams, $filter, data, sessions, obj.student, obj.organisation);
             $scope.searchForm.$invalid = false;
          });
          return;
        }
      };

      $scope.viewSession = function(item) {
        $location.path( "/teachers/"+item.id+"/sessions").search({startDate: $scope.searchForm.startingDate, endDate:$scope.searchForm.endingDate});
      };
    });
  }
]);

function filterSessionByStartTime(startDate, endDate) {
  return firebase.database()
    .ref("session")
    .orderByChild("timeStart")
    .startAt(startDate.getTime() / 1000)
    .endAt(endDate.getTime() / 1000)
    .once("value")
    .then(function(snapshot){
      var sessions = [];
      snapshot.forEach(function(child){
        var val = child.val();
        sessions.push(val);
      });
      return sessions;
    });
}

function createTableData($scope, ngTableParams, $filter, data, sessions, students, org) {
  $scope.validationError = "";
  $scope.showPrintAttendanceButton = true;

  $scope.printAttendance = function() {
    var startDate = new Date($scope.searchForm.startingDate).toLocaleDateString("en-GB", {
      "year": "numeric",
      "month": "numeric",
      "day": "numeric"
    });

    var endDate = new Date($scope.searchForm.endingDate).toLocaleDateString("en-GB", {
      "year": "numeric",
      "month": "numeric",
      "day": "numeric"
    });

    var tableWidth = [];
    var teacherName = [];
    var numberOfSessions = [];
    var body = [];
    var schoolName = org[$scope.reportData[0].orgId].name;
    var numberOfMaxStudent = Math.max.apply(null, $scope.reportData.map(function(item) {
                                return item.studentList.length;
                              }));
    for(var i = 0 ; i < $scope.reportData.length; i++){
      tableWidth.push('*');
      teacherName.push({
        text: $scope.reportData[i].name,
        style: 'header'
      });
      numberOfSessions.push({
        text: 'Total: ' + $scope.reportData[i].numberOfSessions + ' Sessions',
        style: 'header'
      });
    }
    body.push(teacherName);
    body.push(numberOfSessions);
    for(var j = 0 ; j < numberOfMaxStudent; j++){
      var arr = [];
      for(var k = 0 ; k < $scope.reportData.length; k++){
        var studentId = $scope.reportData[k].studentList[j];
        arr.push((studentId) ? students[studentId].name : "");
      }
      body.push(arr);
    }

    var docDefinition = {
     content: [
       {
         text: schoolName,
         style: 'titleHeader'
       },
       {
         text: 'Start Date: ' + startDate,
         style: 'date'
       },
       {
         text: 'End Date: ' + endDate,
         style: 'date'
       },
       {
         style: 'demoTable',
         table: {
           widths: tableWidth,
           body: body
         }
       }
     ],
     styles: {
       header: {
         bold: true,
         color: '#000',
         fontSize: 14
       },
       demoTable: {
         color: '#666',
         fontSize: 10
       },
       date:{
    			fontSize: 12,
    			margin: [0, 0, 0, 10]
    		},
       titleHeader:{
    			fontSize: 18,
    			bold: true,
    			margin: [0, 0, 0, 10]
    		}
     }
   };
   pdfMake.createPdf(docDefinition).download('report.pdf');
 };
  $scope.makeSearch = function() {
      $scope.reportData = [];
      $scope.validationError = "";
      $scope.tableParams = new ngTableParams({
        page: 1, // show first page
        count: 10,
        sorting: {
          name: 'asc' // initial sorting
        }
      }, {
        filterDelay: 50,
        total: data.length, // length of data
        getData: function($defer, params) {
          var searchStr = $scope.searchForm.searchTerm;
          var mydata = [];
          if (searchStr) {
            searchStr = searchStr.toLowerCase();
            mydata = data.filter(function(item) {
              return item.name.toLowerCase().indexOf(searchStr) > -1;
            });
            populateTableData($scope, mydata, sessions);
          } else {
            mydata = data;
            populateTableData($scope, mydata, sessions);
          }
          mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
          $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
      });
  };
}

function populateTableData($scope, mydata, sessions) {
  var sessionByStaffId = {};
  var enabledPrintAttendanceButton;

  if(mydata.length) {
    for (var i in mydata) {
       var filteredItem = mydata[i];
       var studentList = [];
       for (var j = 0; j < Object.keys(sessions).length; j++) {
         var session = sessions[Object.keys(sessions)[j]] || '';
         if(session.staffId === filteredItem.id) {
             sessionByStaffId[filteredItem.id] = (sessionByStaffId[filteredItem.id] || 0) + 1;
             studentList.push(session.studentId);
             delete sessionByStaffId[undefined];
         }
       }

       if(Object.keys(sessionByStaffId).indexOf(filteredItem.id) > -1){
         enabledPrintAttendanceButton = true;
         var unique = studentList.filter(function(item, i, ar){ return ar.indexOf(item) === i; });
         filteredItem.numberOfSessions = sessionByStaffId[filteredItem.id];
         filteredItem.studentList = unique;
         $scope.reportData.push(filteredItem);
       } else {
         filteredItem.numberOfSessions = 0;
       }
    }
    if(enabledPrintAttendanceButton) {
      $scope.showPrintAttendanceButton = false;
    } else {
      $scope.showPrintAttendanceButton = true;
    }
  } else {
    $scope.showPrintAttendanceButton = true;
  }
}

// active tabs
function tab_active() {
  $('.org-tab').on('click', function() {
    $(this).addClass("active");
  });
}
