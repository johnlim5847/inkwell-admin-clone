app.controller('OrganizationController', ['$scope', "$firebaseObject", "$firebaseArray", 'ngTableParams', '$filter','$routeParams', 'Flash', function($scope, $firebaseObject, $firebaseArray, ngTableParams, $filter, $routeParams, Flash){

  // Firebase Setup
  var ref = firebase.database().ref();
  var list = $firebaseArray(ref);
  var obj = $firebaseObject(ref);
  $('.loaderdiv').show();
  $('.orgData').hide();
  // to take an action after the data loads, use the $loaded() promise
  obj.$loaded().then(function() {
    $('.loaderdiv').hide();
    $('.orgData').show();
    $scope.name = list[0][Object.keys(obj.organisation)[0]].name;
    $scope.list = list[0];
    $scope.orgPin = list[0][Object.keys(obj.organisation)[0]].org_code;
    var data = [];
    var schools = list[0][Object.keys(obj.organisation)[0]].config.schools;
    var subjects = list[0][Object.keys(obj.organisation)[0]].config.subjects;
    var levels = list[0][Object.keys(obj.organisation)[0]].config.levels;
    var count = 0;
    if($routeParams.tpl == "schools"){
      schools.forEach(function(element) {
        data.push({
          name: element,
          id: count++
        });
      });
    }else if($routeParams.tpl == "subjects"){
      subjects.forEach(function(element) {
        data.push({
          name: element,
          id: count++
        });
      });
    }else if($routeParams.tpl == "levels"){
      levels.forEach(function(element) {
        data.push({
          name: element,
          id: count++
        });
      });
    }
    dataList($scope, ngTableParams, $filter, data);
    $scope.BasicInfoSave = function(name, orgPin) {
      list[0][Object.keys(obj.organisation)[0]].name = name;
      list[0][Object.keys(obj.organisation)[0]].org_code = orgPin;
      list.$save(0);
      var message = 'Info Successfully Updated';
      var id = Flash.create('success', message, 4000, {}, false);
    };
    $scope.modalValue = function(item){
      $scope.value = item;
      $scope.id = item.id;
    };
    $scope.OrgInfoSave = function(name, type, id){
      if(type == "school"){
        if (typeof id == 'undefined'){
          list[0][Object.keys(obj.organisation)[0]].config.schools.push(name);
          $scope.data.push({
            name: name
          });
          $scope.tableParams.reload();
        }else{
          list[0][Object.keys(obj.organisation)[0]].config.schools[id] = name;
        }
      }else if(type == "subject"){
        if(typeof id == 'undefined'){
          list[0][Object.keys(obj.organisation)[0]].config.subjects.push(name);
          $scope.data.push({
            name: name
          });
          $scope.tableParams.reload();
        }else{
          list[0][Object.keys(obj.organisation)[0]].config.subjects[id] = name;
        }
      }else if(type == "level"){
        if(typeof id == 'undefined'){
          list[0][Object.keys(obj.organisation)[0]].config.levels.push(name);
          $scope.data.push({
            name: name
          });
          $scope.tableParams.reload();
        }else{
          list[0][Object.keys(obj.organisation)[0]].config.levels[id] = name;
        }
      }
      list.$save(0);
      var message = '';
      if(typeof id == 'undefined'){
        message = type.charAt(0).toUpperCase()+type.slice(1)+' Successfully Created';
      }else{
        message = type.charAt(0).toUpperCase()+type.slice(1)+' Successfully Updated';
      }
      Flash.create('success', message, 4000, {}, false);
    };
  });
}]);

function dataList($scope, ngTableParams, $filter, data){
  $scope.data = data;
  $scope.tableParams = new ngTableParams({
    page: 1,            // show first page
    count: 10,
    sorting: {
      name: 'asc'     // initial sorting
    }
  }, {
    filterDelay: 50,
    total: data.length, // length of data
    getData: function($defer, params) {
      var searchStr = params.filter().search;
      var mydata = [];
      console.log(data);
      if(searchStr){
        searchStr = searchStr.toLowerCase();
        mydata = data.filter(function(item){
          return item.name.toLowerCase().indexOf(searchStr) > -1;
        });
      } else {
        mydata = data;
      }
      mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
      $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
    }
  });
}

// active tabs
function tab_active() {
  $('.org-tab').on('click',function(){
      $(this).addClass("active");
  });
}