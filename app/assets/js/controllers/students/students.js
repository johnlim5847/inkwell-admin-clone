app.controller('StudentController', ['$scope', "$firebaseObject", "$firebaseArray", 'ngTableParams', '$filter','$routeParams', 'Flash', 'studentService', '$location', function($scope, $firebaseObject, $firebaseArray, ngTableParams, $filter, $routeParams, Flash, studentService, $location){

  // Firebase Setup
  var ref = firebase.database().ref();
  var list = $firebaseArray(ref);
  var obj = $firebaseObject(ref);
  $('.loaderdiv').show();
  $('.studentData').hide();
  // to take an action after the data loads, use the $loaded() promise
  obj.$loaded().then(function() {
    if($routeParams.tpl == "basic-info"){
      $scope.infoDiv = true;
      $scope.studentsDiv = false;
    }else if($routeParams.folder == "students"){
      $scope.infoDiv = false;
      $scope.studentsDiv = true;
    }
    $scope.schools = list[0][Object.keys(obj.organisation)[0]].config.schools;
    $scope.levels = list[0][Object.keys(obj.organisation)[0]].config.levels;
    $scope.studentStatus = ["active", "archive"];
    $scope.students_test = obj.student;
    $scope.sessions_test = obj.transaction;
    $('.loaderdiv').hide();
    $('.studentData').show();
    $scope.studentStatusActive = true;
    $scope.studentStatusArchive = true;
    var data = [];
    var students = obj.school;
    var count = 0;
    console.log();
    if($routeParams.id){
      console.log("ok");
    }
    $scope.value_id = $routeParams.id;
    $scope.value = studentService.getStudents();
    console.log($routeParams);
    if($routeParams.folder == "students"){
      for (var key in obj.student){
        if($routeParams.id == key){
          $scope.value = obj.student[key];
        }
        data.push({
            name: obj.student[key].name,
            school: obj.student[key].school,
            level: obj.student[key].level,
            status: obj.student[key].status,
            balance: obj.student[key].balance,
            id: key
          });
      }
    }
    dataList($scope, ngTableParams, $filter, data);
    $scope.modalValue = function(item){
      $scope.value = item;
      $scope.item = item.id;
    };
    $scope.changeSidebar = function(item){
      studentService.addStudent(item, item.id);
      $scope.value = item;
      $scope.infoDiv = true;
      $scope.studentsDiv = false;
      $location.path( "/students/"+item.id+"/basic-info" );
    };
    $scope.tab_change = function(item){
      console.log(item);
      studentService.addStudent(item);
      $('.student-tab').on('click',function(){
        $(this).addClass("active");
      });
    };
    $scope.delete = function(item){
      $scope.data = data;
      delete obj.student[item.id];
      $scope.data.splice($scope.data.indexOf(item), 1);
      $scope.tableParams.reload();
      Flash.create('success', "Student Successfully Deleted", 4000, {}, false);
      obj.$save("student").then(function (ref) {
        console.log("Saved !");
      }, function (error) {
        console.log("Error:", error);
      });
    };
    $scope.StudentInfoSave = function(value, id){
      if (typeof id == 'undefined'){
        var newObj = {};
        newObj = {
          name: value.name,
          school: value.school,
          level: value.level,
          status: value.status,
          balance: value.balance,
          email: "",
          orgId: "",
          phoneNumber: "",
          profileImage: "",
          userId: ""};
        obj.student[value.name.replace(/\s+/g, '-').toLowerCase()] = newObj;
        $scope.data.push(newObj);
        $scope.tableParams.reload();
      }else{
        obj.student[id].name = value.name;
        obj.student[id].school = value.school;
        obj.student[id].level = value.level;
        obj.student[id].status = value.status;
        obj.student[id].balance = value.balance;
      }
      obj.$save("student").then(function (ref) {
        console.log("Saved !");
      }, function (error) {
        console.log("Error:", error);
      });
      var message = '';
      if(typeof id == 'undefined'){
        message = 'Student Successfully Created';
      }else{
        message = 'Student Successfully Updated';
      }
      Flash.create('success', message, 4000, {}, false);
    };


  $scope.selection = ['active', 'archive'];
  $scope.getCheckedActive = function(){
    return true;
  };
  $scope.getCheckedArchive = function(){
    return true;
  };
  // Toggle selection for a given fruit by name
  $scope.toggleSelection = function toggleSelection(active, archive) {
    statusList($scope, ngTableParams, true, data, active, archive);
  };
  });
}]);
function statusList($scope, ngTableParams, filter, data, active, archive){
  var mydata = [];
  mydata = data.filter(function(item){
    if(active && archive ){
      return true;
    }else if(archive && item.status.toLowerCase() == "archive"){
      return true;
    }else if(active && item.status.toLowerCase() == "active"){
      return true;
    }
  });
  $scope.tableParams = new ngTableParams({
    page: 1,            // show first page
    count: 10           // count per page
  },{
    total: data.length, // length of data
    getData: function($defer, params) {
      $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
    }
  });
}

function dataList($scope, ngTableParams, $filter, data){
  $scope.data = data;
  $scope.tableParams = new ngTableParams({
    page: 1,            // show first page
    count: 10,
    sorting: {
      name: 'asc'     // initial sorting
    }
  }, {
    filterDelay: 50,
    total: data.length, // length of data
    getData: function($defer, params) {
      var searchStr = params.filter().search;
      var mydata = [];
      if(searchStr){
        searchStr = searchStr.toLowerCase();
        mydata = data.filter(function(item){
          return item.name.toLowerCase().indexOf(searchStr) > -1;
        });
      } else {
        mydata = data;
      }
      mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
      $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
    }
  });
}

// active tabs
function tab_active() {
  $('.student-tab').on('click',function(){
      $(this).addClass("active");
  });
}
