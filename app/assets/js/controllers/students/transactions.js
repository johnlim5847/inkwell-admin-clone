app.controller('TransactionController', ['$scope', "$firebaseObject", "$firebaseArray", 'ngTableParams', '$filter','$routeParams', 'Flash', 'studentService', function($scope, $firebaseObject, $firebaseArray, ngTableParams, $filter, $routeParams, Flash, studentService){

  // Firebase Setup
  var ref = firebase.database().ref();
  var list = $firebaseArray(ref);
  var obj = $firebaseObject(ref);
  $scope.infoDiv = true;
  $('.loaderdiv').show();
  $('.studentData').hide();
  // to take an action after the data loads, use the $loaded() promise
  obj.$loaded().then(function() {
    $scope.value = studentService.getStudents();
    $scope.methods = ["Cheque", "Cash", "Bank Transfer", "Adjustment", "Session", "Cancel Session"];
    $scope.staffs = obj.staff;
    $scope.students = obj.student;
    // console.log(obj.staff);
    $scope.transactions = obj.transaction;
    $('.loaderdiv').hide();
    $('.studentData').show();
    var data = [];
    var transaction = obj.transaction;
    var count = 0;

    $scope.value_id = $routeParams.id;
    if($routeParams.tpl == "transactions"){
      for (var key in obj.transaction){
        if($routeParams.id == obj.transaction[key].studentId){
          $scope.value = obj.student[obj.transaction[key].studentId];
          data.push({
            amount: obj.transaction[key].amount,
            studentId: obj.student[obj.transaction[key].studentId],
            createdBy: obj.staff[obj.transaction[key].createdBy],
            method: obj.transaction[key].method,
            id: key
          });
        }
      }
    }
    dataList($scope, ngTableParams, $filter, data);
    $scope.modalValue = function(item){
      $scope.value = item;
      $scope.item = item.id;
    };
    $scope.tab_change = function(item){
      studentService.addStudent(item);
      $('.student-tab').on('click',function(){
        $(this).addClass("active");
      });
    };
    $scope.TransactionInfoSave = function(value, id){
      var staffKey = 0;
      var studentKey = 0;
      for (key in obj.staff){
        if(obj.staff[key].name == value.createdBy.name){
          staffKey = key;
        }
      }
      for (var key in obj.student){
        if(obj.student[key].name == value.studentId.name){
          studentKey = key;
        }
      }
      if (typeof id == 'undefined'){
        // var newObj = {};
        // newObj = {
        //   timeStart: Date.parse(value.timeStart),
        //   subject: value.subject,
        //   staffId: staffKey,
        //   level: value.level,
        //   units: value.units
        // };
        // obj.session[value.timeStart+value.school] = newObj;
        // $scope.data.push(newObj);
        // $scope.tableParams.reload();
      }else{
        // console.log(value.subject);
        obj.transaction[id].amount = value.amount;
        obj.transaction[id].studentId = studentKey;
        obj.transaction[id].createdBy = staffKey;
        obj.transaction[id].method = value.method;
      }
      obj.$save("transaction").then(function (ref) {
        console.log("Saved !");
      }, function (error) {
        console.log("Error:", error);
      });
      var message = '';
      if(typeof id == 'undefined'){
        message = 'Transaction Successfully Created';
      }else{
        message = 'Transaction Successfully Updated';
      }
      Flash.create('success', message, 4000, {}, false);
    };


  $scope.selection = ['active', 'archive'];
  $scope.getCheckedActive = function(){
    return true;
  };
  $scope.getCheckedArchive = function(){
    return true;
  };
  // Toggle selection for a given fruit by name
  $scope.toggleSelection = function toggleSelection(status) {
    var idx = $scope.selection.indexOf(status);
    console.log(idx);
    // Is currently selected
    if (idx > -1) {
      if(status == 'active'){
        $scope.getCheckedArchive = function(){
          return true;
        };
      }
      if(status == 'archive'){
        $scope.getCheckedActive = function(){
          return true;
        };
      }
      statusList($scope, ngTableParams, status, data, false);
      $scope.selection.splice(idx, 1);
    }
    // Is newly selected
    else {
      if(status == 'active'){
        $scope.getCheckedArchive = function(){
          return false;
        };
      }
      if(status == 'archive'){
        $scope.getCheckedActive = function(){
          return false;
        };
      }
      statusList($scope, ngTableParams, status, data, true);
      $scope.selection.push(status);
    }
  };
  });
}]);
// function statusList($scope, ngTableParams, value, data, checked){
//   var mydata = [];
//   mydata = data.filter(function(item){
//     if(checked){
//       return item.status.toLowerCase() == value.toLowerCase();
//     }else{
//       return item.status.toLowerCase() != value.toLowerCase();
//     }
//   });
//   $scope.tableParams = new ngTableParams({
//     page: 1,            // show first page
//     count: 10           // count per page
//   },{
//     total: data.length, // length of data
//     getData: function($defer, params) {
//       $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
//     }
//   });
// }

function dataList($scope, ngTableParams, $filter, data){
  $scope.data = data;
  $scope.tableParams = new ngTableParams({
    page: 1,            // show first page
    count: 10,
    sorting: {
      subject: 'asc'     // initial sorting
    }
  }, {
    filterDelay: 50,
    total: data.length, // length of data
    getData: function($defer, params) {
      var searchStr = params.filter().search;
      var mydata = [];
      if(searchStr){
        searchStr = searchStr.toLowerCase();
        mydata = data.filter(function(item){
          return item.subject.toLowerCase().indexOf(searchStr) > -1;
        });
      } else {
        mydata = data;
      }
      mydata = params.sorting() ? $filter('orderBy')(mydata, params.orderBy()) : mydata;
      $defer.resolve(mydata.slice((params.page() - 1) * params.count(), params.page() * params.count()));
    }
  });
}

// active tabs
function tab_active() {
  $('.student-tab').on('click',function(){
      $(this).addClass("active");
  });
}