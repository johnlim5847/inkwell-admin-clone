app.controller('MainController',
  ['$scope', '$animate', 'localStorageService', '$alert', '$rootScope','$location', 'userService','$timeout',
  function($scope, $animate, localStorageService, $alert, $rootScope, $location, userService, $timeout){

  $scope.theme_colors = [
    'pink','red','purple','indigo','blue',
    'light-blue','cyan','teal','green','light-green',
    'lime','yellow','amber','orange','deep-orange'
  ];
  if(localStorage.getItem("email")){
    $scope.userName = localStorage.getItem("email").split("@", 1).join();
  }else{
    $scope.userName = $rootScope.userName;
  }

  $scope.fillinContent = function(){
    $scope.htmlContent = 'content content';
  };

  // theme changing
  $scope.changeColorTheme = function(cls){
    $rootScope.$broadcast('theme:change', 'Choose template');//@grep dev
    $scope.theme.color = cls;
  };

  $scope.signOut = function(event) {
    // event.preventDefault();  // To prevent form refresh
    firebase.auth().signOut();
    localStorage.clear();
    $location.path("/login");
  };

  $scope.changeTemplateTheme = function(cls){
    $rootScope.$broadcast('theme:change', 'Choose color');//@grep dev
    $scope.theme.template = cls;
  };

  if ( !localStorageService.get('theme') ) {
    theme = {
      color: 'theme-pink',
      template: 'theme-template-dark'
    };
    localStorageService.set('theme', theme);
  }
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      $scope.userName = user.email.split("@", 1).join();
      $('.side_menu').show();
    } else {
      $('.side_menu').hide();
    }
  });
  localStorageService.bind($scope, 'theme');
}]);