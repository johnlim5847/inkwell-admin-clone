app.controller('DashboardController',
  ['$window', '$scope', '$rootScope', '$interval', 'colorService',"$firebaseObject", "$firebaseArray",
  function($window, $scope, $rootScope, $interval, colorService, $firebaseObject, $firebaseArray){

  // Firebase Setup
  var ref = firebase.database().ref();
  var list = $firebaseArray(ref);
  var obj = $firebaseObject(ref);
  obj.$loaded().then(function() {
    $rootScope.pageTitle = 'Dashboard';

    $scope.dashboardData = {};
    var sessions = list[1];
    var staff = list[2];
    var transactions = list[4];

    var currentDate = new Date();  //test: new Date('Sat Feb 25 2017');
    var currentMonth = currentDate.getMonth();
    var currentDay = currentDate.toDateString();
    var firstDay = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);

    // Helper Functions for Graph
    Date.prototype.addDays = function(days) {
        var dat = new Date(this.valueOf());
        dat.setDate(dat.getDate() + days);
        return dat;
    };

    function genDates(startDate, stopDate) {
        var dateArray = {};
        var day = startDate;
        while (day <= stopDate) {
            var dat = new Date(day);
            dateArray[dat.toDateString()] = 0;
            day = day.addDays(1);
        }
        return dateArray;
    }

    // Monthly Total & Daily Total for Transactions
    var monthlyTransactionTotal = 0;
    var dailyTransactionTotal = 0;
    var dailyTotalTransactionObject = genDates(firstDay, currentDate);
    for (var i = 0; i < Object.keys(transactions).length; i++) {
      var transaction = transactions[Object.keys(transactions)[i]] || '';
      var transactionDate = new Date(transaction.dateCreated * 1000);
      var transactionMonth = transactionDate.getMonth();
      var transactionDay = transactionDate.toDateString();
      if (transaction.amount > 0 && currentMonth === transactionMonth) {
        dailyTotalTransactionObject[transactionDay] = (dailyTotalTransactionObject[transactionDay] || 0) + transaction.amount;
        monthlyTransactionTotal+=transaction.amount;
      }
      if (transaction.amount > 0 && currentDay === transactionDay) {
        dailyTransactionTotal+=transaction.amount;
      }
    }

    // Monthly Total & Daily Total for Sessions
    var monthlySessionTotal = 0;
    var dailySessionTotal = 0;
    var dailyTotalSessionObject = genDates(firstDay, currentDate);
    var sessionByStaffId = {};
    for (var j = 0; j < Object.keys(sessions).length; j++) {
      var session = sessions[Object.keys(sessions)[j]] || '';
      var sessionDate = new Date(session.timeStart * 1000);
      var sessionMonth = sessionDate.getMonth();
      var sessionDay = sessionDate.toDateString();
      if (currentMonth === sessionMonth) {
        sessionByStaffId[session.staffId] = (sessionByStaffId[session.staffId] || 0) + 1;
        delete sessionByStaffId[undefined];
        dailyTotalSessionObject[sessionDay] = (dailyTotalSessionObject[sessionDay] || 0) + session.units;

        monthlySessionTotal+=session.units;
      }
      if (currentDay === sessionDay) {
        dailySessionTotal+=session.units;
      }
    }

    // Monthly Top 3 Teachers
    var teacherArrayObject = [];
    for (var k = 0; k < Object.keys(staff).length; k++) {
      var staffList = staff[Object.keys(staff)[k]] || '';
      if (staffList.type === 'Teacher') {
         var userId = Object.keys(staff)[k];
          teacherArrayObject.push({
            name: staffList.name,
            profileImage: staffList.profileImage,
            sessionTaught: sessionByStaffId[userId] || 0
          });
      }
    }

    // Sorting
    teacherArrayObject.sort(function(a, b) {
      return b.sessionTaught - a.sessionTaught;
    });

    // Data for chart.
    var sortedDailyTotalTransactionValues = Object.keys(dailyTotalTransactionObject).sort(function(a,b){
      return new Date(a) - new Date(b);
    }).map(function(el) {
      return dailyTotalTransactionObject[el];
    });

    var sortedDailyTotalTransactionDates = Object.keys(dailyTotalTransactionObject).sort(function(a,b){
      return new Date(a) - new Date(b);
    });

    var sortedDailyTotalSessionValues = Object.keys(dailyTotalSessionObject).sort(function(a,b){
      return new Date(a) - new Date(b);
    }).map(function(el) {
      return dailyTotalSessionObject[el];
    });

    var sortedDailyTotalSessionDates = Object.keys(dailyTotalSessionObject).sort(function(a,b){
      return new Date(a) - new Date(b);
    });

    sortedDailyTotalTransactionValues.unshift('Daily Sales');
    sortedDailyTotalSessionValues.unshift('Daily Sessions');

    $scope.dashboardData.monthlyTransactionTotal = String('$'+monthlyTransactionTotal/100);
    $scope.dashboardData.dailyTransactionTotal = String('$'+dailyTransactionTotal/100);
    $scope.dashboardData.monthlySessionTotal = String(monthlySessionTotal);
    $scope.dashboardData.dailySessionTotal = String(dailySessionTotal);
    $scope.dashboardData.teachers = teacherArrayObject;

    pattern = [];
    pattern.push(colorService.theme());

    $scope.color_pattern = pattern.join();

    sales_chart_options = {
      bindto: '#chart-sales',
      legend: { show: false },
      padding: {
          top: 6,
          right: -1,
          bottom: -8,
          left: 0
      },
      data: {
        columns: [
          sortedDailyTotalTransactionValues
        ],
        type: 'line'
      },
      size: {
        height: 150
      },
      axis: {
        x: {
          show: false,
          padding: {
            left: 2,
            right: 2
          }
        },
        y: {
          show: false,
          padding: {
            top: 5,
            bottom: 5
          }
        }
      },
      grid: {
        focus: { show: false }
      },
      point: { show: true },
      tooltip: {
        format: {
          title: function (x) {
            return sortedDailyTotalTransactionDates[x];
           },
           value: function (x) {
              return '$'+x/100;
           }
        }
      },
      transition: { duration: 50 },
      color: { pattern: ['#fff'] }
    };

    sessions_chart_options = {
      bindto: '#chart-sessions',
      legend: { show: false },
      padding: {
          top: 6,
          right: -1,
          bottom: -8,
          left: 0
      },
      data: {
        columns: [
          sortedDailyTotalSessionValues
        ],
        type: 'line'
      },
      size: {
        height: 150
      },
      axis: {
        x: {
          show: false,
          padding: {
            left: 2,
            right: 2
          }
        },
        y: {
          show: false,
          padding: {
            top: 5,
            bottom: 5
          }
        }
      },
      grid: {
        focus: { show: false }
      },
      point: { show: true },
      tooltip: {
        format: {
          title: function (x) {
            return sortedDailyTotalSessionDates[x];
          }
        }
      },
      transition: { duration: 50 },
      color: { pattern: ['#fff'] }
    };

    var sessions_chart = c3.generate(sessions_chart_options);

    var sales_chart = c3.generate(sales_chart_options);

  });

}]);
