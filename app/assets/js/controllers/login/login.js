// Home controller
angular.module('materialism.home', ['ngRoute']);

// Home controller
app.controller('LoginCtrl', ['$scope','$firebaseAuth','$location', '$rootScope', '$cookies', 'userService',function($scope, $firebaseAuth, $location, $rootScope, $cookies, userService) {
  var auth = $firebaseAuth();
  $scope.userName = "";
  $scope.SignIn = function(event) {
    // event.preventDefault();  // To prevent form refresh
    if(typeof $scope.user != 'undefined'){
      var username = $scope.user.email;
      var password = $scope.user.password;
      auth.$signInWithEmailAndPassword(username, password).then(function(firebaseUser) {
        $cookies.put('firebaseUser',firebaseUser);
        localStorage.setItem("email", username);
        localStorage.setItem("firebaseUser", firebaseUser.id);
        $scope.userName = localStorage.getItem("email").split("@", 1).join();
        userService.addUser(localStorage.getItem("email").split("@", 1).join());
        $location.path("/dashboard");
        $scope.user.email = '';
        $scope.user.password = '';
      }).catch(function(error) {
        $('.login_error').show();
      });
    }
  };
}]);
