app.service('userService', function() {
  var user = {};

  var addUser = function(newObj) {
      user = newObj;
  };

  var getUser = function(){
      return user;
  };

  return {
    addUser: addUser,
    getUser: getUser
  };

});