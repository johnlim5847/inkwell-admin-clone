app.service('studentService', function() {
  var studentList = {};

  var addStudent = function(newObj) {
      studentList = newObj;
  };

  var getStudents = function(){
      return studentList;
  };

  return {
    addStudent: addStudent,
    getStudents: getStudents
  };

});