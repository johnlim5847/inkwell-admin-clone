app.service('teacherService', function() {
  var teacherList = {};

  var addTeacher = function(newObj) {
      teacherList = newObj;
  };

  var getTeachers = function(){
      return teacherList;
  };

  return {
    addTeacher: addTeacher,
    getTeachers: getTeachers
  };

});